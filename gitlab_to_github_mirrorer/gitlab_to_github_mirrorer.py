#!/bin/python3
import attr
from dotenv import load_dotenv
import requests

import os


load_dotenv()


@attr.s
class Project:
    id = attr.ib()
    name = attr.ib()
    group = attr.ib()

    def get_mirror_details(self):
        res = requests.get(
            f"https://gitlab.com/api/v4/projects/{self.id}/remote_mirrors",
            headers=GITLAB_HEADERS,
        )
        if res.json():
            print(self.group, self.name)
            print(res.json())


GITLAB_HEADERS = {"PRIVATE-TOKEN": os.getenv("GITLAB_PRIVATE_TOKEN")}
GITLAB_USERNAME = os.getenv("GITLAB_USERNAME")


projects = []
res = requests.get(
    f"https://gitlab.com/api/v4/users/{GITLAB_USERNAME}/projects",
    headers=GITLAB_HEADERS,
)
for proj in res.json():
    if proj["visibility"] != "private":
        projects.append(Project(proj["id"], proj["name"], GITLAB_USERNAME))
res = requests.get(f"https://gitlab.com/api/v4/groups", headers=GITLAB_HEADERS)
for group in res.json():
    res = requests.get(
        f"https://gitlab.com/api/v4/groups/{group['id']}/projects",
        headers=GITLAB_HEADERS,
    )
    for proj in res.json():
        projects.append(Project(proj["id"], proj["name"], group["name"]))

for project in projects:
    project.get_mirror_details()
# group_res = requests.get(f"http://git.local/api/v4/groups?search={GROUP}", headers=headers)
# project_res = requests.post(f"http://git.local/api/v4/projects", headers=headers, data = {"name": PROJECT, "namespace_id": group_res.json()[0]["id"]})
